import { request } from '@/request/third-party';
import {
  CreateItemsRequest,
  CreateItemsResponse,
  DelData,
  DelRequest,
  GetItemListRequest,
  GetItemListResponse,
  TableConfigData,
  TableConfigRequest,
  TableData,
  TableDataRequest,
  UpdateItemsData,
  UpdateItemsRequest,
} from '@/types/third-party/api';

export function getTableList(data: TableDataRequest) {
  return request<TableData>({ url: '/table/list', method: 'post', data });
}

export function getTableConfig(data: TableConfigRequest) {
  return request<TableConfigData>({
    url: `/table/${data.table_id}`,
    method: 'post',
  });
}

export function updateItems(data: UpdateItemsRequest) {
  return request<UpdateItemsData>({
    url: '/items',
    method: 'put',
    data,
  });
}

export function createItems(data: CreateItemsRequest) {
  return request<CreateItemsResponse>({
    url: '/items',
    method: 'post',
    data,
  });
}

export function getItemList(data: GetItemListRequest) {
  return request<GetItemListResponse>({
    url: '/item/list',
    method: 'post',
    data,
  });
}

export function delItems(data: DelRequest) {
  return request<DelData>({
    url: '/items',
    method: 'delete',
    data,
  });
}
