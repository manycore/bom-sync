import { useModel } from 'umi';

export function ThirdPartyData() {
  const { transformedData } = useModel('syncData');
  const jsonData = JSON.stringify(transformedData, null, 2);

  return <pre>{jsonData}</pre>;
}
