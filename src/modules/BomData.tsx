import { rootFieldNames } from '@/constants/materials';
import { Table } from 'antd';
import { transform } from 'lodash-es';
import { useModel } from 'umi';

interface TableDataItem {
  orderId: string;
  orderName: string;
  materialNumber: number;
}

export function BomData() {
  const { orderResult } = useModel('orderInfo');
  const orderInfo = orderResult.data ?? [];
  const tableData = transform(orderInfo, (result: TableDataItem[], value) => {
    result.push({
      orderId: value.orderId,
      orderName: value.orderName,
      materialNumber: rootFieldNames.reduce(
        (count, name) => count + (value[name]?.length ?? 0),
        0,
      ),
    });
  });

  return (
    <Table
      pagination={false}
      key={'orderId'}
      columns={[
        {
          title: '订单名称',
          dataIndex: 'orderName',
          key: 'orderName',
        },
        {
          title: '物料数',
          dataIndex: 'materialNumber',
          key: 'materialNumber',
        },
      ]}
      dataSource={tableData}
    />
  );
}
