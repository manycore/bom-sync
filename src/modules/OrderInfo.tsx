import { Card, Col, Row, Statistic } from 'antd';
import { useModel } from 'umi';

export function OrderInfo() {
  const { orderResult, bomResult } = useModel('orderInfo');

  return (
    <Row gutter={16}>
      <Col span={12}>
        <Card bordered={false}>
          <Statistic
            title="订单总数"
            value={orderResult.data?.totalCount}
            loading={orderResult.loading}
            valueStyle={{ color: '#3f8600' }}
          />
        </Card>
      </Col>
      <Col span={12}>
        <Card bordered={false}>
          <Statistic
            title="物料数"
            value={bomResult.data?.totalCount}
            loading={bomResult.loading}
            valueStyle={{ color: '#cf1322' }}
          />
        </Card>
      </Col>
    </Row>
  );
}
