import { getItemList, getTableConfig } from '@/api';
import { GetItemListRequest } from '@/types/third-party/api';
import { useRequest } from 'ahooks';
import { Button, Form, Input, Select, Table } from 'antd';
import { assign, isEmpty, negate } from 'lodash-es';
import { useModel } from 'umi';

export function LinkOrder() {
  const { mainTableId: tableId, setLinkedOrder } = useModel('fieldConfig');
  const { data: tableConfig } = useRequest(
    async () => {
      // console.log({ tableId });
      if (!isEmpty(tableId)) {
        return getTableConfig({ table_id: tableId! });
      }
    },
    {
      refreshDeps: [tableId],
    },
  );
  // console.log({ tableConfig });
  const fieldOptions = tableConfig?.table.fields.map((field) => {
    return {
      label: field.name,
      value: field.field_id,
    };
  });

  const { data, run: fetchData } = useRequest(
    async (params?: { field_id: string; field_value: string }) => {
      // console.log({ tableId, params });
      if (!isEmpty(tableId)) {
        const requestParams: GetItemListRequest = {
          table_id: tableId!,
        };
        if (params) {
          const { field_id, field_value } = params;
          if ([field_id, field_value].every(negate(isEmpty))) {
            assign(requestParams, {
              filter: {
                and: [
                  {
                    field: field_id,
                    query: {
                      eq: field_value,
                    },
                  },
                ],
              },
            });
          }
        }
        return getItemList(requestParams);
      }
    },
    { refreshDeps: [tableId] },
  );
  // console.log(data);
  const tableData = data?.items.map((item) => {
    return {
      id: item.item_id,
      name: item.title,
    };
  });

  // console.log({ data });

  const [form] = Form.useForm();
  // console.log(form, form.getFieldsValue());

  return (
    <div>
      <Form
        className="mb-2"
        form={form}
        layout="inline"
        onFinish={(values) => {
          // console.log(values);
          fetchData(values);
        }}
      >
        <Form.Item name={['field_value']}>
          <Input
            addonBefore={
              <Form.Item noStyle name={['field_id']}>
                <Select style={{ width: 98 }} options={fieldOptions} />
              </Form.Item>
            }
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            查询
          </Button>
        </Form.Item>
      </Form>

      <Table
        pagination={false}
        rowKey={'id'}
        rowSelection={{
          type: 'radio',
          onChange(selectedRowKeys, selectedRows, info) {
            console.log({ selectedRowKeys, selectedRows, info }); //
            const [id] = selectedRowKeys;
            setLinkedOrder(id as string);
          },
        }}
        className="overflow-auto"
        scroll={{ y: 220 }}
        columns={[
          {
            title: '名称',
            dataIndex: 'name',
          },
          {
            title: 'ID',
            dataIndex: 'id',
          },
        ]}
        dataSource={tableData}
      />
    </div>
  );
}
