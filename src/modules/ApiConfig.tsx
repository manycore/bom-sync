import { storeKey } from '@/config/api-config';
import { useLocalStorageState } from 'ahooks';
import { Button, Form, Input } from 'antd';
import React from 'react';

type FieldType = {
  apiKey?: string;
  spaceId?: string;
};

export const ApiConfig: React.FC = () => {
  const [apiConfig, setApiConfig] = useLocalStorageState<FieldType>(storeKey);
  function onFinish(values: FieldType) {
    setApiConfig(values);
    location.reload();
  }

  return (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      style={{ maxWidth: 600 }}
      initialValues={apiConfig}
      onFinish={onFinish}
      autoComplete="off"
    >
      <Form.Item<FieldType>
        label="ApiKey"
        name="apiKey"
        rules={[{ required: true, message: 'Please input your apikey!' }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item<FieldType>
        label="SpaceId"
        name="spaceId"
        rules={[{ required: true, message: 'Please input your spaceId!' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );
};
