import { getTableConfig } from '@/api';
import { getTableBodyHeight } from '@/utils';
import { useBoolean, useRequest, useSize } from 'ahooks';
import { Button, Modal, Table } from 'antd';
import { isEmpty } from 'lodash-es';
import { ComponentRef, useRef, useState } from 'react';
import { useModel } from 'umi';
import { FieldConfig } from './FieldConfig';

type TableRef = ComponentRef<typeof Table>;
export function ThirdPartyTableConfig() {
  const {
    tableId,
    mainFieldId,
    setMainFieldId,
    thirdFieldConfig,
    setThirdFieldConfig,
  } = useModel('fieldConfig');
  function updateThirdFieldConfig(
    tableId: string,
    fieldId: string,
    path: string,
  ) {
    if (thirdFieldConfig[tableId] === undefined) {
      thirdFieldConfig[tableId] = {};
    }
    thirdFieldConfig[tableId][fieldId] = path;
    setThirdFieldConfig(thirdFieldConfig);
  }
  const ref = useRef<TableRef>(null);
  const size = useSize(ref.current?.nativeElement);
  const tableBodyHeight =
    size && getTableBodyHeight(size.height, { hasFooter: false });
  const { data } = useRequest(
    async () => {
      console.log({ tableId });
      if (!isEmpty(tableId)) {
        return getTableConfig({ table_id: tableId });
      }
    },
    { refreshDeps: [tableId] },
  );
  // console.log(data);
  const thirdPartyTableId = data?.table.table_id;
  const tableData = data?.table.fields.map((item) => {
    return {
      id: item.field_id,
      name: item.name,
    };
  });

  const [open, { set: setOpen }] = useBoolean();
  const [thirdPartyFieldId, setThirdPartyFieldId] = useState<string>();
  const onOk = () => {
    setOpen(false);
  };
  const onCancel = () => {
    setOpen(false);
  };

  function onConfig(id: string) {
    console.log({ id });
    setThirdPartyFieldId(id);
    setOpen(true);
  }

  return (
    <>
      <Table
        pagination={false}
        rowKey={'id'}
        rowSelection={{
          type: 'radio',
          onChange(selectedRowKeys, selectedRows, info) {
            console.log({ selectedRowKeys, selectedRows, info }); //
          },
        }}
        ref={ref}
        className="overflow-auto"
        scroll={{ y: tableBodyHeight }}
        columns={[
          {
            title: '表名',
            dataIndex: 'name',
          },
          {
            title: '字段',
            dataIndex: 'id',
          },
          {
            title: 'BOM字段',
            dataIndex: 'bomFieldId',
            render(_, record) {
              return thirdFieldConfig[thirdPartyTableId!]?.[record.id];
            },
          },
          {
            title: '操作',
            render(_, record) {
              return (
                <Button type="primary" onClick={() => onConfig(record.id)}>
                  配置
                </Button>
              );
            },
          },
        ]}
        dataSource={tableData}
      ></Table>
      <Modal open={open} title="配置" onOk={onOk} onCancel={onCancel}>
        <FieldConfig
          className="h-64 overflow-auto"
          onSelect={(bomFieldId) => {
            updateThirdFieldConfig(
              thirdPartyTableId!,
              thirdPartyFieldId!,
              bomFieldId,
            );
          }}
        />
      </Modal>
    </>
  );
}
