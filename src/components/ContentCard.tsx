import { Card } from 'antd';
import { PropsWithChildren } from 'react';

export function ContentCard(props: PropsWithChildren<{ title: string }>) {
  return (
    <Card
      bordered={false}
      className="w-1/3"
      bodyStyle={{
        height: 'calc(100% - 56px)',
        display: 'flex',
        justifyContent: 'center',
      }}
      {...props}
    />
  );
}
