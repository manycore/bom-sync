import store from 'store2';

interface ApiConfig {
  apiKey: string;
  spaceId: string;
}

export const storeKey = 'apiConfig';

export function updateApiConfig(params: ApiConfig) {
  store.set(storeKey, params);
}

export function getApiConfig() {
  return store.get(storeKey) as ApiConfig;
}
