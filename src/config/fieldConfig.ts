import { FieldConfig } from '@/types';
import fieldConfigJson from './data/fieldConfig.json';

export const fieldConfig: FieldConfig = fieldConfigJson;
