import {
  ApiConfig,
  BomData,
  LinkOrder,
  SyncData,
  ThirdPartyData,
  ThirdPartyTableConfig,
  ThirdPartyTableList,
} from '@/modules';
import { Card, Divider, Flex } from 'antd';

export default function HomePage() {
  return (
    <div className="bg-slate-50 p-8">
      <Card title="接口配置">
        <ApiConfig />
      </Card>
      <Divider />
      {[
        [
          {
            title: 'BOM数据',
            children: <BomData />,
          },
          {
            title: '伙伴云数据',
            children: <ThirdPartyData />,
          },
        ],
        [
          {
            title: '数据表选择',
            children: <ThirdPartyTableList />,
          },
          {
            title: '数据配置',
            children: <ThirdPartyTableConfig />,
          },
          {
            title: '关联订单',
            children: <LinkOrder />,
          },
        ],
      ].map((row) => {
        return (
          <Flex gap="small" className="h-96 mb-2">
            {row.map(
              (props: {
                title: string;
                children: React.ReactNode;
                className?: string;
              }) => {
                return (
                  <Card
                    bordered={false}
                    className={props.className ?? 'w-1/2'}
                    bodyStyle={{
                      height: 'calc(100% - 56px)',
                      display: 'flex',
                      justifyContent: 'center',
                    }}
                    {...props}
                  />
                );
              },
            )}
          </Flex>
        );
      })}
      <Divider />
      <SyncData />
    </div>
  );
}
