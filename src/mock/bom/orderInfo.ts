import { Materials, OrderInfoItem } from '@/types/orderInfo';
import { merge } from 'lodash-es';
import _materials from './materials.json';

export const orderIds = ['3FO4K4WPMP95'];

export const materials: Materials = _materials as unknown as Materials;

export const orderInfo: OrderInfoItem[] = [
  merge({ orderId: 'xxx', orderName: 'mock' }, materials),
];
