import { transform } from 'lodash-es';

export function getPathValue(obj: object, path: string) {
  const parts = path.split('/');
  const result = transform(
    obj,
    (result, part) => {
      if (part in result) {
        result = result[part];
      }
    },
    {},
  );
  return result;
}
