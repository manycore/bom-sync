export function getTableBodyHeight(
  height: number,
  { hasHeader = true, hasFooter = true } = {},
) {
  return height - (hasHeader ? 56 : 0) - (hasFooter ? 64 : 0);
}
