interface TreeNode {
  [key: string]: TreeNode;
}

function buildTree(paths: string[]): TreeNode {
  const tree: TreeNode = {};

  for (const path of paths) {
    const parts = path.split('/');
    let node = tree;

    for (const part of parts) {
      if (!(part in node)) {
        node[part] = {};
      }
      node = node[part];
    }
  }

  return tree;
}

const serializeTree = (node?: TreeNode): string => {
  if (!node) {
    return '';
  }

  const children = [];

  for (const [key, subtree] of Object.entries(node)) {
    const serializedSubtree = serializeTree(subtree);

    if (serializedSubtree !== '') {
      children.push(`${key}(${serializedSubtree})`);
    } else {
      children.push(key);
    }
  }

  return children.join(',');
};

function compressPaths(paths: string[]): string {
  const tree = buildTree(paths);
  return serializeTree(tree);
}

export { compressPaths as treePath2MaskPath };

// const paths = ["inners", "inners/points", "inners/curves", "inners/curves/type", "inners/curves/bulge", "edgeBanding"];
// const compressed = compressPaths(paths);
// console.log(compressed);
