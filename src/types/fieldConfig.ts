export interface FieldConfig {
  [key: string]: FieldConfig | null;
}

export type BomFieldId = string;
export type ThirdPartyFieldId = string;
export interface ThirdPartyFieldConfig {
  [thirdPartyTableId: string]: {
    [thirdPartyFieldId: string]: BomFieldId;
  };
}
