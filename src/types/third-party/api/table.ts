export interface TableDataRequest {
  /**
   * 工作区ID
   */
  space_id: string;
  [property: string]: any;
}

/**
 * 返回数据，返回具体数据的结构
 */
export interface TableData {
  tables: TableDataItem[];
  [property: string]: any;
}

/**
 * 表格
 */
export interface TableDataItem {
  /**
   * 表格别名，在工作区内唯一，未设置别名的返回空字符串
   */
  alias: string;
  /**
   * 创建时间，格式为：YYYY-MM-DD HH:ii
   */
  created_on: string;
  /**
   * 表格名称
   */
  name: string;
  /**
   * 工作区ID
   */
  space_id: string;
  /**
   * 表格ID
   */
  table_id: string;
  [property: string]: any;
}

export interface TableConfigRequest {
  /**
   * 表格ID
   */
  table_id: string;
  [property: string]: any;
}

/**
 * 返回数据，返回具体数据的结构
 */
export interface TableConfigData {
  table: TableConfig;
  [property: string]: any;
}

/**
 * 表格配置
 */
export interface TableConfig {
  /**
   * 表格别名，在工作区内唯一，未设置别名的返回空字符串
   */
  alias: string;
  /**
   * 创建时间，格式为：YYYY-MM-DD HH:ii
   */
  created_on: string;
  /**
   * 字段列表
   */
  fields: FieldConfig[];
  /**
   * 表格名称
   */
  name: string;
  /**
   * 工作区ID
   */
  space_id: string;
  /**
   * 表格ID
   */
  table_id: string;
  [property: string]: any;
}

/**
 * 字段配置
 */
export interface FieldConfig {
  /**
   * 字段别名，在表格内唯一
   */
  alias: string;
  /**
   * 字段配置
   */
  config: FieldList;
  /**
   * 数据类型
   */
  data_type: DataType;
  /**
   * 描述
   */
  description: string;
  /**
   * 字段ID
   */
  field_id: string;
  /**
   * 字段类型
   */
  field_type: FieldType;
  /**
   * 来自关联的附加，当前字段来自于关联字段的附加，只显示字段值，不可写入字段值
   */
  from_relation_field: FromRelationField;
  /**
   * 字段名称
   */
  name: string;
  /**
   * 是否必填
   */
  required: boolean;
  [property: string]: any;
}

/**
 * 字段配置
 *
 * 字段列表
 *
 * numeric 数字类型
 *
 * date 日期类型
 *
 * category 选项类型
 *
 * user 用户类型
 *
 * relation 关联类型
 *
 * location 位置类型
 *
 * file 文件类型
 */
export interface FieldList {
  /**
   * 按百分比显示，仅数值字段返回，是：1，否：0
   */
  is_percent?: string;
  /**
   * 小数位数，留空代表按数值原样保存和显示，不强制处理小数位
   *
   * 日期精度，仅日期：date，日期和时间：datetime
   */
  precision?: number | string;
  /**
   * 单位前缀名称，百分比固定为空，其他任意填写
   */
  unit_prefix?: string;
  /**
   * 单位后缀名称，百分比固定为“%”，其他任意填写
   */
  unit_surfix?: string;
  /**
   * 多选，是：1，否：0
   */
  is_multi?: number;
  /**
   * 平铺显示，是：1，否：0
   */
  is_tile?: number;
  /**
   * 备选项
   */
  options?: Option[];
  /**
   * 多选，是：1，否：0
   *
   * 上传多张，是：1，否：0
   */
  is_mutil?: number;
  /**
   * 关联表格所在的工作区ID
   */
  space_id?: string;
  /**
   * 关联的表格ID
   */
  table_id?: string;
  /**
   * 只能获取当前位置，是：1，否：0
   */
  is_current?: number;
  /**
   * 添加水印，仅图片字段返回，是：1，否：0
   */
  is_watermark?: number;
  [property: string]: any;
}

/**
 * 备选项
 */
export interface Option {
  /**
   * 备选项ID，在单个备选项字段内唯一，全局不唯一
   */
  id: string;
  /**
   * 备选项名称，在单个备选项字段内唯一，全局不唯一
   */
  name: string;
  [property: string]: any;
}

/**
 * 数据类型
 */
export enum DataType {
  Category = 'category',
  Date = 'date',
  File = 'file',
  Location = 'location',
  Numeric = 'numeric',
  Relation = 'relation',
  Signature = 'signature',
  Text = 'text',
  User = 'user',
}

/**
 * 字段类型
 */
export enum FieldType {
  Barcode = 'barcode',
  Calculation = 'calculation',
  Category = 'category',
  Date = 'date',
  File = 'file',
  Image = 'image',
  Input = 'input',
  Location = 'location',
  Money = 'money',
  Number = 'number',
  Numeric = 'numeric',
  Relation = 'relation',
  Rich = 'rich',
  Signature = 'signature',
  Textarea = 'textarea',
  User = 'user',
}

/**
 * 来自关联的附加，当前字段来自于关联字段的附加，只显示字段值，不可写入字段值
 */
export interface FromRelationField {
  /**
   * 字段ID，所属的关联字段的字段ID
   */
  field_id: string;
  [property: string]: any;
}
