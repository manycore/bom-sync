/**
 * Request
 */
export interface UpdateItemsRequest {
  /**
   * 数据，更新或创建的数据列表
   */
  items: Item[];
  /**
   * 表格ID
   */
  table_id: string;
  /**
   * 更新依据字段，可指定多个更新依据的系统字段和自定义字段的 ID 列表，进行联合唯一判断，如填写了“item_id”则会忽略传入的其他字段，仅使用“item_id”作为更新依据
   */
  update_by_fields: string[];
  /**
   * 更新模式
   */
  update_type: UpdateType;
  [property: string]: any;
}

interface Item {
  /**
   * 字段值，如在“更新依据字段”中指定了对应的字段则必须提供该字段及值
   */
  fields: Fields;
  /**
   * 数据ID，如在“更新依据字段”中指定了 item_id 则必须提供，仅用作更新数据的依据字段
   */
  item_id?: string;
  [property: string]: any;
}

/**
 * 字段值，如在“更新依据字段”中指定了对应的字段则必须提供该字段及值
 */
interface Fields {
  /**
   * 只枚举出需要提交的字段，Key为字段ID，并根据字段的数据类型（data_type）提交对应类型的字段值
   */
  '{field_id}': Array<FieldidObject | string> | number | 位置 | string;
  [property: string]: any;
}

/**
 * 文件，提交文件列表，请调用文件上传接口获取文件ID
 *
 * 文件
 */
interface FieldidObject {
  /**
   * 文件ID
   */
  file_id: string;
  /**
   * 文件名称
   */
  name: string;
  [property: string]: any;
}

/**
 * 位置，提交位置信息
 *
 * 位置，已选的位置信息
 *
 * 签名，提交签名图片
 */
interface 位置 {
  /**
   * 区编码，来自高德地图
   */
  adcode?: string;
  /**
   * 地址
   */
  address?: string;
  /**
   * 区名称
   */
  adname?: string;
  /**
   * 城市编码，来自高德地图
   */
  citycode?: string;
  /**
   * 城市名称
   */
  cityname?: string;
  /**
   * 经纬度，来自高德地图
   */
  coordinate?: Coordinate;
  /**
   * 全地址，兴趣点名+地址
   */
  name?: string;
  /**
   * 省编码，来自高德地图
   */
  pcode?: string;
  /**
   * 省名称
   */
  pname?: string;
  /**
   * 兴趣点名称
   */
  poi?: string;
  /**
   * 签名图片，签名图片的二进制代码
   */
  image?: string;
  [property: string]: any;
}

/**
 * 经纬度，来自高德地图
 */
interface Coordinate {
  /**
   * 纬度
   */
  lat: number;
  /**
   * 经度
   */
  lon: number;
  [property: string]: any;
}

/**
 * 更新模式
 */
export enum UpdateType {
  Update = 'update',
  Upsert = 'upsert',
}

export interface UpdateItemsData {
  [k: string]: never;
}
