export interface CreateItemsRequest {
  /**
   * 结果回调回传字段，如果配置了该值，则回调结果中会返回配置的字段的值，最多设置3个字段ID
   */
  callbackFieldids?: string[];
  /**
   * 数据
   */
  items: 字段提交[];
  /**
   * 表格ID
   */
  table_id: string;
  [property: string]: any;
}

/**
 * 字段提交
 */
export interface 字段提交 {
  /**
   * 只枚举出需要提交的字段，Key为字段ID，并根据字段的数据类型（data_type）提交对应类型的字段值
   */
  fieldid: Array<FieldidObject | string> | number | 位置 | string;
  [property: string]: any;
}

/**
 * 位置，提交位置信息
 *
 * 位置，已选的位置信息
 *
 * 签名，提交签名图片
 */
export interface 位置 {
  /**
   * 区编码，来自高德地图
   */
  adcode?: string;
  /**
   * 地址
   */
  address?: string;
  /**
   * 区名称
   */
  adname?: string;
  /**
   * 城市编码，来自高德地图
   */
  citycode?: string;
  /**
   * 城市名称
   */
  cityname?: string;
  /**
   * 经纬度，来自高德地图
   */
  coordinate?: Coordinate;
  /**
   * 全地址，兴趣点名+地址
   */
  name?: string;
  /**
   * 省编码，来自高德地图
   */
  pcode?: string;
  /**
   * 省名称
   */
  pname?: string;
  /**
   * 兴趣点名称
   */
  poi?: string;
  /**
   * 签名图片，签名图片的二进制代码
   */
  image?: string;
  [property: string]: any;
}

/**
 * 经纬度，来自高德地图
 */
export interface Coordinate {
  /**
   * 纬度
   */
  lat: number;
  /**
   * 经度
   */
  lon: number;
  [property: string]: any;
}

/**
 * 返回数据，返回具体数据的结构
 */
export interface CreateItemsResponse {
  /**
   * 数据列表
   */
  items: Item[];
  [property: string]: any;
}

export interface Item {
  /**
   * 数据ID
   */
  itemid: number;
  [property: string]: any;
}

/**
 * 字段值，如在“更新依据字段”中指定了对应的字段则必须提供该字段及值
 */
export interface Fields {
  /**
   * 只枚举出需要提交的字段，Key为字段ID，并根据字段的数据类型（data_type）提交对应类型的字段值
   */
  '{field_id}': Array<FieldidObject | string> | number | 位置 | string;
  [property: string]: any;
}

/**
 * 文件，提交文件列表，请调用文件上传接口获取文件ID
 *
 * 文件
 */
export interface FieldidObject {
  /**
   * 文件ID
   */
  file_id: string;
  /**
   * 文件名称
   */
  name: string;
  [property: string]: any;
}

export interface GetItemListRequest {
  /**
   * 筛选器，结构详见”开发指南 → 数据筛选器“说明文档
   */
  filter?: Connector;
  /**
   * 每页条数，默认 20 条，最大 100 条
   */
  limit?: number;
  /**
   * 分页偏移量，默认为 0
   */
  offset?: number;
  /**
   * 排序
   */
  order?: Order;
  /**
   * 表格ID
   */
  table_id: string;
  [property: string]: any;
}

/**
 * 筛选器，结构详见”开发指南 → 数据筛选器“说明文档
 *
 * Connector
 *
 * 且
 *
 * And
 */
export interface Connector {
  /**
   * 且
   */
  and?: Condition[];
  [property: string]: any;
}

/**
 * 查询条件
 *
 * Condition
 *
 * 或
 *
 * Or
 *
 * 且
 *
 * And
 */
export interface Condition {
  /**
   * 字段标识
   */
  field?: string;
  query?: Query;
  /**
   * 或
   */
  or?: Condition[];
  /**
   * 且
   */
  and?: Condition[];
  [property: string]: any;
}

/**
 * Query
 *
 * Eqm
 *
 * Eq
 *
 * Ne
 *
 * Gt
 *
 * Gte
 *
 * Lt
 *
 * Lte
 *
 * In
 *
 * Ain
 *
 * Nin
 *
 * Inc
 *
 * Em
 */
export interface Query {
  /**
   * 全部匹配关键字中任意一个
   */
  eqm?: string[];
  /**
   * 等于，代表”=”操作
   */
  eq?: string;
  /**
   * 不等于，代表”!=”操作
   */
  ne?: string;
  /**
   * 大于，代表”>”操作
   */
  gt?: number;
  /**
   * 大于等于，代表”>=”操作
   */
  gte?: number;
  /**
   * 小于，代表”<”操作
   */
  lt?: number;
  /**
   * 小于等于，代表”<=”操作
   */
  lte?: number;
  /**
   * 包含，代表”IN”操作，主要用于数组类型字段，判断数组是否包含某个值
   */
  in?: number | string;
  /**
   * 包含全部关键字
   */
  ain?: string[];
  /**
   * 不包含，代表”NOT IN”操作，主要用于数组类型字段，判断数组是否不包含某个值
   */
  nin?: number | string;
  /**
   * 在...中，查询是否为给定数组的子集
   */
  inc?: number[];
  /**
   * 是否存在，代表”IS NULL”操作，判断某个值是否为null，注意：空字符串!=null
   */
  em?: string;
  [property: string]: any;
}

/**
 * 排序
 */
export interface Order {
  /**
   * 排序字段ID
   */
  field_id: string;
  /**
   * 排序类型，正序：asc，倒序：desc
   */
  type: string;
  [property: string]: any;
}

/**
 * 返回数据，返回具体数据的结构
 */
export interface GetItemListResponse {
  /**
   * 数据列表
   */
  items: 数据[];
  [property: string]: any;
}

/**
 * 数据
 */
export interface 数据 {
  created_by: { [key: string]: any };
  /**
   * 创建时间，格式为：YYYY-MM-DD HH:ii
   */
  created_on: string;
  /**
   * 字段值
   */
  fields: { [key: string]: any }[];
  /**
   * 数据ID
   */
  item_id: string;
  /**
   * 数据标题
   */
  title: string;
  updated_by: { [key: string]: any };
  /**
   * 修改时间，格式为：YYYY-MM-DD HH:ii
   */
  updated_on: string;
  [property: string]: any;
}
