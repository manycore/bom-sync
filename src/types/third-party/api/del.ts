export interface DelRequest {
  /**
   * 删除的数据ID列表
   */
  item_ids: string[];
  /**
   * 表格ID
   */
  table_id: string;
  [property: string]: any;
}

export interface DelData {
  [key: string]: any;
}
