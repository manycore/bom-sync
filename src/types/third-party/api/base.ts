export interface Response<T = any> {
  /**
   * 错误码，0 为成功，小于 10000 为系统异常，其他为业务异常
   */
  code: number;
  /**
   * 返回数据，返回具体数据的结构
   */
  data: T;
  /**
   * 错误信息，具体的错误原因，code 为 0 时返回“success”
   */
  message: string;
  /**
   * 元数据，用来描述返回数据原始信息
   */
  meta: Meta;
  [property: string]: any;
}

/**
 * 元数据，用来描述返回数据原始信息
 */
export interface Meta {
  /**
   * 请求ID，每次请求接口会返回唯一的ID
   */
  trace_id: string;
  [property: string]: any;
}
