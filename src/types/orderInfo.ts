export type Materials = Awaited<
  ReturnType<typeof Manycore.Integration.Bom.findMaterialsAsync>
>;

export interface OrderInfoItem extends Materials {
  orderId: string;
  orderName: string;
  // planks: object[];
}
