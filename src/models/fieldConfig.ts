import { fieldConfig as _fieldConfig } from '@/config/fieldConfig';
import { ThirdPartyFieldConfig } from '@/types';
import { useLocalStorageState, useRequest } from 'ahooks';
import { useState } from 'react';

export default function fieldConfig() {
  const [tableId, setTableId] = useState<string>('');
  const [mainTableId, setMainTableId] =
    useLocalStorageState<string>('mianTableId');
  const [mainFieldId, setMainFieldId] =
    useLocalStorageState<string>('mainFieldId');
  const [linkedOrder, setLinkedOrder] = useState<string>();
  const [thirdFieldConfig = {}, setThirdFieldConfig] =
    useLocalStorageState<ThirdPartyFieldConfig>('thirdFieldConfig');
  const fieldConfig = useRequest(async () => {
    return _fieldConfig;
  });
  const [exportedFields, setExportedFields] = useState<string[]>([]);

  return {
    tableId,
    setTableId,
    mainTableId: mainTableId,
    setMainTable: setMainTableId,
    mainFieldId,
    setMainFieldId,
    linkedOrder,
    setLinkedOrder,
    fieldConfig: fieldConfig.data,
    exportedFields,
    setExportedFields,
    thirdFieldConfig,
    setThirdFieldConfig,
  };
}
