import { useState } from 'react';

export default function syncData() {
  const [transformedData, setTransformedData] = useState<object>({});
  return { transformedData, setTransformedData };
}
