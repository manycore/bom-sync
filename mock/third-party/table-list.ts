import { defineMock } from 'umi';
import tableList from './table-list.json';

export default defineMock({
  'post /api/table/list': tableList,
});
