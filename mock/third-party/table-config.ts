import { defineMock } from 'umi';
import tableConfig from './table-config.json';

export default defineMock({
  'post /api/table/2100000055205113': tableConfig,
});
