import { defineConfig } from 'umi';

export default defineConfig({
  routes: [{ path: '/', component: 'index' }],
  antd: {},
  locale: {
    antd: true,
  },
  model: {},

  npmClient: 'pnpm',
  tailwindcss: {},
  plugins: [
    '@umijs/plugins/dist/tailwindcss',
    '@umijs/plugins/dist/locale',
    '@umijs/plugins/dist/antd',
    '@umijs/plugins/dist/model',
  ],
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  history: {
    type: 'hash',
  },
  verifyCommit: { allowEmoji: true },
  proxy: {
    '/prod-api': {
      target: 'https://api.huoban.com/openapi/v1/',
      changeOrigin: true,
      pathRewrite: { '^/prod-api': '' },
    },
  },
});
